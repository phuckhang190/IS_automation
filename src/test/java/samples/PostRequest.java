package samples;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PostRequest {
    @Test
    public void FirstPostRequest(){
        RestAssured.baseURI = "http://restapi.demoqa.com/customer";
        RequestSpecification request = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("FirstName","Virender");
        requestParams.put("LastName","Singh");
        requestParams.put("UserName","simpleuser001");
        requestParams.put("Password","password1");
        requestParams.put("Email","someuser@gmail.com");

        request.header("Content-Type","application/json");
        request.body(requestParams.toJSONString());
        Response response = request.post("/register");

        System.out.println("Response content is: " + response.getBody().asString());
        int statusCode = response.getStatusCode();
        Assert.assertEquals(response.getStatusCode(),"200");

        String successCode = response.jsonPath().get("SuccessCode");
        Assert.assertEquals(successCode, "OPERATION_SUCCESS");
    }

    @Test
    public void RegistrationSuccessful(){
        RestAssured.baseURI = "http://restapi.demoqa.com/customer/register";
        RequestSpecification request = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("FirstName","Virender");
        requestParams.put("LastName","Singh");
        requestParams.put("UserName","simpleuser001");
        requestParams.put("Password","password1");
        requestParams.put("Email","someuser@gmail.com");

        request.header("Content-Type","application/json");
        request.body(requestParams.toJSONString());
        Response response = request.post();

        ResponseBody body = response.getBody();
        System.out.println(body.asString());

        if(response.statusCode() == 200){
            RegistrationFailureResponse responseBody = body.as(RegistrationFailureResponse.class);
            Assert.assertEquals(responseBody.FaultId,"User already exists");
            Assert.assertEquals(responseBody.fault,"FAULT_USER_ALREADY_EXISTS");
        }else if(response.statusCode() == 201){
            RegistrationSuccessResponse responseBody = body.as(RegistrationSuccessResponse.class);
            Assert.assertEquals(responseBody.SuccessCode, "OPERATION_SUCCESS");
            Assert.assertEquals(responseBody.Message, "Operation completed successfully");
        }

    }
}
