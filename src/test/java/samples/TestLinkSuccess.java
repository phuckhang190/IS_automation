package samples;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestLinkSuccess {

    @Test
    public void TestCheckLinkNotExisted(){
        RestAssured.baseURI = "http://giro.airpay.vn";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type","application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("bank_id","14");
        requestParams.put("client_id","3");
        requestParams.put("ref","84969511112");
        request.body(requestParams.toJSONString());
        Response response = request.post("/check_link/");

        JsonPath jsonPathEvaluator = response.jsonPath();
        String errorCode = jsonPathEvaluator.get("error_code");
        System.out.println("Error code received from Response is: " + errorCode);
        Assert.assertEquals(errorCode,"37");
    }

    @Test(groups = "LinkSuccess")
    public void TestLinkSuccess(){
        RestAssured.baseURI = "http://giro.airpay.vn";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type","application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("bank_id","14");
        requestParams.put("client_id","3");
        requestParams.put("card_number","001704060015454");
        requestParams.put("phone_number","84902883119");
        requestParams.put("ref","84902883119");
        requestParams.put("id_number","2719123456");
        request.body(requestParams.toJSONString());
        Response response = request.post("/link/");

        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals(jsonPath.get("error_code"),"04");
    }

    @Test(groups = "LinkSuccess")
    public void TestVerifyOTP(){
        RestAssured.baseURI = "http://giro.airpay.vn";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type","application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("bank_id","14");
        requestParams.put("client_id","3");
        requestParams.put("ref","84902883119");
        requestParams.put("otp","otp123456");
        request.body(requestParams.toJSONString());
        Response response = request.post("/check_otp/");

        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals(jsonPath.get("error_code"),"00");
    }
}
