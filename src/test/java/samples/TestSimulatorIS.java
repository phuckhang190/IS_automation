package samples;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestSimulatorIS {

    public static Pattern ptn = Pattern.compile("name='csrfmiddlewaretoken' value='(.+?)'");

    public static String captureValueString(String largeText){
        Matcher mtch = ptn.matcher(largeText);
        String ips = new String();
        while(mtch.find()){
            ips = mtch.group(1);
        }
        return ips;
    }

    public static List<String> captureValuesArray(String largeText){
        Matcher mtch = ptn.matcher(largeText);
        List<String> ips = new ArrayList<String>();
        while(mtch.find()){
            ips.add(mtch.group(1));
        }
        return ips;
    }

    public String getAccessToken;

    public String Get_Token_Login_IS(){
        RestAssured.baseURI = "http://ved_test_api.com/admin/login";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type","application/json");
        Response response = request.get();
        System.out.println("response is: " + captureValueString(response.asString()));
        return captureValueString(response.asString());
    }

    @Test
    public void Test_Login_IS(){
        RestAssured.baseURI = "http://ved_test_api.com/admin/login/";
        RequestSpecification request = RestAssured.given();

        String getToken = captureValueString(request.get().asString());
        System.out.println("getToken is: " + getToken);

        JSONObject requestParams = new JSONObject();
        requestParams.put("csrfmiddlewaretoken",getToken);
        requestParams.put("username","admin");
        requestParams.put("password","v3d_t3st_ap1@123");
//        requestParams.put("next","/admin/Server/responsedata/268/");

        request.header("Content-Type","application/json");
        request.body(requestParams.toJSONString());
        Response response1 = request.post();

        System.out.println("Following are response1: " + response1.asString());
        System.out.println("response1 is: " + captureValueString(response1.asString()));
        System.out.println("response1 code is: " + response1.getStatusCode());
//        return captureValueString(response1.asString());
    }

    @Test(enabled = false)
    public void Test_Modify_VIB_Link_IS_550(){
        String reponseData = "{\"StatusCode\":\"000001\",\"ContractID\":\"20180521138442\",\"RequestID\":\"000032\",\"OTP\":\"612522\"}";

        RestAssured.baseURI = "http://ved_test_api.com";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type","application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("csrfmiddlewaretoken","");
        requestParams.put("api_id","550");
        requestParams.put("response_data",reponseData);
        requestParams.put("description","[VIB] LINK");
        requestParams.put("_continue","Save and continue editing");
        requestParams.put("id_number","2719123456");
        request.body(requestParams.toJSONString());
        Response response = request.post("/admin/Server/responsedata/268/");
        System.out.println(response.getStatusCode());

    }


}
