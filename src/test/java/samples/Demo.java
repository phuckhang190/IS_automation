package samples;

import org.testng.annotations.Test;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.jayway.restassured.RestAssured.given;

public class Demo {

    public static Pattern ptn = Pattern.compile("name='csrfmiddlewaretoken' value='(.+?)'");

    public static String captureValueString(String largeText){
        Matcher mtch = ptn.matcher(largeText);
        String ips = new String();
        while(mtch.find()){
            ips = mtch.group(1);
        }
        return ips;
    }

    public static void setupProxy(){
        System.setProperty("http.proxyHost", "127.0.0.1");
        System.setProperty("https.proxyHost", "127.0.0.1");
        System.setProperty("http.proxyPort", "8888");
        System.setProperty("https.proxyPort", "8888");
    }

    public void updateResponseData(String responseData) {
        setupProxy();
        com.jayway.restassured.RestAssured.baseURI = "http://ved_test_api.com";
        com.jayway.restassured.response.Cookie cookie = given().when().get("/admin/login/").getDetailedCookie("csrftoken");
        Map<String, String> r = given()
                .header("Host", "ved_test_api.com")
                .header("Connection", "keep-alive")
//                .header( "Content-Length", "112")
                .header("Cache-Control", "max-age=0")
                .header("Origin", "http://ved_test_api.com")
                .header("Upgrade-Insecure-Requests", "1")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36")
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .header("Referer", "http://ved_test_api.com/admin/login/?next=/admin/")
                .header("Accept-Encoding", "gzip, deflate")
                .header("Accept-Language", "en,en-US;q=0.9,vi;q=0.8")
                .header("Cookie", "csrftoken=" + cookie.getValue())
                .with().parameters("csrfmiddlewaretoken", cookie.getValue(), "username", "admin", "password", "getPassword", "next", "admin")
                .cookie(cookie).when().post("/admin/login/?next=/admin/").thenReturn().getCookies();

        com.jayway.restassured.response.Response r2 = given().cookie("csrftoken", r.get("csrftoken")).cookie("sessionid", r.get("sessionid")).get("/admin");

        com.jayway.restassured.response.Response r3 = given()
                .header("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary0USX0Tcon6khang1")
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .cookie("csrftoken", r.get("csrftoken")).cookie("sessionid", r.get("sessionid"))
                .multiPart("csrfmiddlewaretoken", r.get("csrftoken"))
                .multiPart("api_id", "550")
                .multiPart("response_data", responseData)
                .multiPart("description", "[VIB] LINK")
                .multiPart("_continue", "Save and continue editing")
                .post("/admin/Server/responsedata/268/");
    }

}
