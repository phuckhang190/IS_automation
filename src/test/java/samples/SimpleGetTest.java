package samples;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleGetTest {

    @Test(enabled = false)
    public void GetWeatherDetails(){
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.request(Method.GET, "/Hyderabad");
        String responseBody = response.getBody().asString();
        System.out.println("Response Body is =>" + responseBody);

        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200, "Correct status code has returned");
        System.out.println("Server returns statusCode = " + statusCode);
    }

    @Test(enabled = false)
    public void GetWeatherWithWrongCity(){
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.request(Method.GET, "/123456789");
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode,200,"Correct status code has returned");
    }

    @Test(enabled = false)
    public void GetWeatherStatusLine(){
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.request(Method.GET, "/Hyderabad");
        String statusLine = response.getStatusLine();
        Assert.assertEquals(statusLine, "HTTP/1.1 200 OK", "Correct status line has returned");
        System.out.println("Server returns status line = " + statusLine);
    }

    @Test(enabled = false)
    public void GetWeatherHeaders(){
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get("/Hyderabad");

        String contentType = response.getHeader("Content-Type");
        System.out.println("Content-Type value is: " + contentType);

        String serverType = response.getHeader("Server");
        System.out.println("Server value is: " + serverType);

        String acceptLanguage = response.getHeader("Content-Encoding");
        System.out.println("Content-Encoding value is: " + acceptLanguage);

        Headers allHeaders = response.headers();
        for (Header head : allHeaders){
            System.out.println("Key: " + head.getName() + " & Value: " + head.getValue());
        }
    }

    @Test
    public void WeatherMessageBogy(){
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get("/Hyderabad");

        ResponseBody body = response.getBody();
        System.out.println("Response body is: " + body.asString());
    }

    @Test
    public void VerifyCityInJsonResponse(){
        RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get("/Hyderabad");

        JsonPath jsonPathEvaluator = response.jsonPath();
        String city = jsonPathEvaluator.get("City");
        System.out.println("City received from Response: " + city);

        Assert.assertEquals(city,"Hyderabad", "Correct city name received in the Response");
    }
}
