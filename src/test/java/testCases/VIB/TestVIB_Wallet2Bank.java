package testCases.VIB;

import libraries.FuncHandler;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestVIB_Wallet2Bank extends FuncHandler {

    @Test(dataProvider = "dataProVIBW2BMapping")
    public void TestVIBSimulatorW2B(String isGetCode, String clientGetCode) throws IOException, ParseException {
        String responseData = "{\"RequestID\":\"000044\",\"StatusCode\":\"" + isGetCode + "\",\"Transnum\":\"2018052103098313\"}";
        updateResponseData(ApiID.VIB_W2B.toString(), responseData);

        String responseCode = simulatorW2B(BankName.VIB.toString());
        Assert.assertEquals(responseCode, clientGetCode);
    }

    @Test
    public void TestVIBSimulatorW2BTimeOut() throws IOException, ParseException {
        /**Perform a link*/
        String strUpdateResponse = "{\"StatusCode\":\"000000\",\"ContractID\":\"20180521138442\",\"RequestID\":\"000032\",\"OTP\":\"612522\"}";
        updateResponseData(ApiID.VIB_LINK.toString(), strUpdateResponse);
        linkGiroSimulatorSuccessful(BankName.VIB.toString());

        String responseData = "sleep|90|{\"RequestID\":\"000044\",\"StatusCode\":\"000000\",\"Transnum\":\"2018052103098313\"}";
        updateResponseData(ApiID.VIB_W2B.toString(), responseData);
        String responseCode = simulatorW2B(BankName.VIB.toString());
        Assert.assertEquals(responseCode, "36");
    }

//    I'm practicing the git branch
}
