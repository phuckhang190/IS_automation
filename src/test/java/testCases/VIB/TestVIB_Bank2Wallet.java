package testCases.VIB;

import libraries.FuncHandler;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestVIB_Bank2Wallet extends FuncHandler {

    @Test(dataProvider = "dataProVIBB2WMapping", groups = "B2WSimulator")
    public void TestVIBSimulatorB2W(String ISGetCode, String ClientGetCode) throws IOException, ParseException {
        String responseData = "{\"RequestID\":\"000042\",\"StatusCode\":\""+ISGetCode+"\",\"Transnum\":\"2018052103098312\",\"TransResult\":\"INIT\",\"OTP\":\"170669\"}";
        updateResponseData(ApiID.VIB_B2W.toString(), responseData);

        String responseCode = simulatorB2W(BankName.VIB.toString());
        Assert.assertEquals(responseCode, ClientGetCode);
    }

    @Test
    public void TestVIBSimulatorB2WTimeOut() throws IOException, ParseException {
        /**Perform a link*/
        String strUpdateResponse = "{\"StatusCode\":\"000000\",\"ContractID\":\"20180521138442\",\"RequestID\":\"000032\",\"OTP\":\"612522\"}";
        updateResponseData(ApiID.VIB_LINK.toString(), strUpdateResponse);
        linkGiroSimulatorSuccessful(BankName.VIB.toString());

        String responseData = "sleep|90|{\"RequestID\":\"000042\",\"StatusCode\":\"000000\",\"Transnum\":\"2018052103098312\",\"TransResult\":\"INIT\",\"OTP\":\"170669\"}";
        updateResponseData(ApiID.VIB_B2W.toString(), responseData);
        String responseCode = simulatorB2W(BankName.VIB.toString());
        Assert.assertEquals(responseCode, "42");
    }

}
