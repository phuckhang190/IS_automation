package testCases.VIB;

import libraries.FuncHandler;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestVIB_Link extends FuncHandler {

    @Test(dataProvider = "dataProVIBLink", groups = "LinkBank")
    public void TestVIBBankLink(String bankId, String clientId, String cardNumber, String phoneNumber, String refNumber, String idNumber, String errorCode) {
        verifyLinkApiWithBank(bankId, clientId, cardNumber, phoneNumber, refNumber, idNumber, errorCode);
    }

    @Test(dataProvider = "dataProVIBLinkMapping", groups = "LinkSimulator")
    public void TestVIBSimulatorLink(String ISGetCode, String ClientGetCode) throws IOException, ParseException {
        String responseData = "{\"StatusCode\":\"" + ISGetCode + "\",\"ContractID\":\"20180521138442\",\"RequestID\":\"000032\",\"OTP\":\"612522\"}";
        updateResponseData(ApiID.VIB_LINK.toString(), responseData);
        String responseCode = verifyLinkApiWithSimulator(BankName.VIB.toString());
        Assert.assertEquals(responseCode, ClientGetCode);
    }

    @Test(dataProvider = "dataProVIBVerifyOTPMapping", groups = "LinkOTPSimulator")
    public void TestVIBSimulatorLinkOTP(String ISGetCode, String ClientGetCode) throws IOException, ParseException {
        String responseData = "{\"StatusCode\":\"" + ISGetCode + "\",\"ContractID\":\"20180521138444\",\"RequestID\":\"000036\"}";
        updateResponseData(ApiID.VIB_LINK_OTP.toString(), responseData);
        String responseCode = VerifyOtpApiWithSimulator(BankName.VIB.toString());
        Assert.assertEquals(responseCode, ClientGetCode);
    }

    @Test
    public void TestVIBSimulatorLinkOTPTimeOut() throws IOException, ParseException {
        /**Perform a link*/
        String strUpdateResponse = "{\"StatusCode\":\"000000\",\"ContractID\":\"20180521138442\",\"RequestID\":\"000032\",\"OTP\":\"612522\"}";
        updateResponseData(ApiID.VIB_LINK.toString(), strUpdateResponse);
        linkGiroSimulatorSuccessful(BankName.VIB.toString());

        String responseDataOTP = "sleep|90|{\"RequestID\":\"000043\",\"StatusCode\":\"000000\",\"Transnum\":\"2018052103098312\"}";
        updateResponseData(ApiID.VIB_LINK_OTP.toString(), responseDataOTP);
        String responseCodeOTP = VerifyOtpApiWithSimulator(BankName.VIB.toString());
        Assert.assertEquals(responseCodeOTP, "41");
    }
}
