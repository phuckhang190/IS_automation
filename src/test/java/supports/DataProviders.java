package supports;

import org.json.simple.parser.ParseException;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class DataProviders extends JsonReader {
    public enum BankName {
        VIB("vib"), SEABANK("seabank");
        private String text;

        BankName(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum ApiID {
        VIB_LINK("550"), VIB_LINK_OTP("551"), VIB_B2W("553"), VIB_B2W_OTP("554"), VIB_W2B("555"), VIB_CHECK_TXN("556"), VIB_UNLINK("557"), VIB_CHECK_BALANCE("558");
        private String text;

        ApiID(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    @DataProvider
    public Object[][] dataProVIBLink() throws Exception {
        Object[][] testObjArray = ExcelUtils.getTableArray("DataVIBBankLink.xlsx", "Sheet1");
        return (testObjArray);
    }

    @DataProvider
    public Object[][] dataProVIBLinkMapping() throws IOException, ParseException {
        return convertJsonToArray("DataVIBSimulatorLink.json", 2);
    }

    @DataProvider
    public Object[][] dataProVIBVerifyOTPMapping() throws IOException, ParseException {
        return convertJsonToArray("DataVIBSimulatorVerifyOTP.json", 2);
    }

    @DataProvider
    public Object[][] dataProVIBB2WMapping() throws IOException, ParseException {
        return convertJsonToArray("DataVIBSimulatorB2W.json", 2);
    }

    @DataProvider
    public Object[][] dataProVIBW2BMapping() throws IOException, ParseException {
        return convertJsonToArray("DataVIBSimulatorW2B.json", 2);
    }
}
