package supports;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class JsonReader {
    private static final String FILE_PATH = "./src/test/java/datas/";

    public static JSONObject getJsonInfo(String fileName) throws IOException, ParseException {
        FileReader reader = new FileReader(FILE_PATH + fileName);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        return jsonObject;
    }

    public static String[][] convertJsonToArray(String fileName, int colNumber) throws IOException, ParseException {
        JSONObject jsonObj = getJsonInfo(fileName);
        String[][] tabArray = null;
        Object[] a = jsonObj.keySet().toArray();
        tabArray = new String[a.length][colNumber];
        for (int i = 0; i < a.length; i++) {
            tabArray[i][0] = a[i].toString();
            tabArray[i][1] = jsonObj.get(a[i]).toString();
        }
        return tabArray;
    }
}
