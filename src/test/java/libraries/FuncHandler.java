package libraries;

import samples.Demo;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import supports.DataProviders;
import supports.JsonReader;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class FuncHandler extends DataProviders {
    public FuncHandler() {
        super();
    }

    public void updateResponseData(String api_id, String responseData) throws IOException, ParseException {
        //Read JSON file
        JSONObject jo = JsonReader.getJsonInfo("API_id.json");
        String urlPath = com.jayway.jsonpath.JsonPath.read(jo, "$." + api_id + ".urlPath");
        String description = com.jayway.jsonpath.JsonPath.read(jo, "$." + api_id + ".description");

        RestAssured.baseURI = "http://ved_test_api.com";
        com.jayway.restassured.response.Cookie cookie = given().when().get("/admin/login/").getDetailedCookie("csrftoken");
        Map<String, String> r = given()
                .header("Host", "ved_test_api.com")
                .header("Connection", "keep-alive")
//                .header( "Content-Length", "112")
                .header("Cache-Control", "max-age=0")
                .header("Origin", "http://ved_test_api.com")
                .header("Upgrade-Insecure-Requests", "1")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36")
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .header("Referer", "http://ved_test_api.com/admin/login/?next=/admin/")
                .header("Accept-Encoding", "gzip, deflate")
                .header("Accept-Language", "en,en-US;q=0.9,vi;q=0.8")
                .header("Cookie", "csrftoken=" + cookie.getValue())
                .with().parameters("csrfmiddlewaretoken", cookie.getValue(), "username", "admin", "password", "v3d_t3st_ap1@123", "next", "admin")
                .cookie(cookie).when().post("/admin/login/?next=/admin/").thenReturn().getCookies();

        com.jayway.restassured.response.Response r3 = given()
                .header("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundary0USX0Tcon6khang1")
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .cookie("csrftoken", r.get("csrftoken")).cookie("sessionid", r.get("sessionid"))
                .multiPart("csrfmiddlewaretoken", r.get("csrftoken"))
                .multiPart("api_id", api_id)
                .multiPart("response_data", responseData)
                .multiPart("description", description)
                .multiPart("_save", "Save")
                .post("/admin/Server/responsedata/" + urlPath + "/");
    }

    private JsonPath linkGiro(String bankId, String clientId, String cardNumber, String phoneNumber, String refNumber, String idNumber) {
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> link = new HashMap<String, String>();
        link.put("bank_id", bankId);
        link.put("client_id", clientId);
        link.put("card_number", cardNumber);
        link.put("phone_number", phoneNumber);
        link.put("ref", refNumber);
        link.put("id_number", idNumber);
        Response rb = given().contentType("application/json")
                .body(link)
                .when().post("/link/");
        JsonPath jp = rb.jsonPath();
        return jp;
    }

    public void linkGiroSimulatorSuccessful(String bankName) throws IOException, ParseException {
        JSONObject jo = JsonReader.getJsonInfo("BanksSimulator.json");
        String bankId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".bank_id");
        String clientId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".client_id");
        String cardNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".cardNumber");
        String phoneNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".phoneNumber");
        String refNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".ref");
        String idNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".idNumber");
        String otp = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".otp");

        /**Link if there is not a linked GIRO*/
        if (!verifyLinkIsExisted(bankId, clientId, refNumber)) {
            linkGiro(bankId, clientId, cardNumber, phoneNumber, refNumber, idNumber);
            otpLinkGiro(bankId, clientId, refNumber, otp);
        }

    }

    public String verifyLinkApiWithSimulator(String bankName) throws IOException, ParseException {
        JSONObject jo = JsonReader.getJsonInfo("BanksSimulator.json");
        String bankId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".bank_id");
        String clientId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".client_id");
        String cardNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".cardNumber");
        String phoneNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".phoneNumber");
        String refNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".ref");
        String idNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".idNumber");

        /**Unlink if the link is existed*/
        if (verifyLinkIsExisted(bankId, clientId, refNumber)) {
            unLink(bankId, clientId, refNumber);
        }

        JsonPath jsonPath = linkGiro(bankId, clientId, cardNumber, phoneNumber, refNumber, idNumber);
        return jsonPath.get("error_code");
    }

    public void verifyLinkApiWithBank(String bankId, String clientId, String cardNumber, String phoneNumber, String refNumber, String idNumber, String errorCode) {
        /**Unlink if the link is existed*/
        if (verifyLinkIsExisted(bankId, clientId, refNumber)) {
            unLink(bankId, clientId, refNumber);
        }
        JsonPath jsonPath = linkGiro(bankId, clientId, cardNumber, phoneNumber, refNumber, idNumber);
        Assert.assertEquals(jsonPath.get("error_code"), errorCode);
    }

    private String otpLinkGiro(String bankId, String clientId, String refNumber, String otp) throws IOException, ParseException {
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> verifyOTP = new HashMap<String, String>();
        verifyOTP.put("bank_id", bankId);
        verifyOTP.put("client_id", clientId);
        verifyOTP.put("ref", refNumber);
        verifyOTP.put("otp", otp);
        JsonPath rb = given().contentType("application/json")
                .body(verifyOTP)
                .when().post("/check_otp/").jsonPath();
        String error_code = rb.get("error_code");
        return error_code;
    }

    private String otpB2WGiro(String bankId, String clientId, String clientTxnId, String refNumber, String otp) throws IOException, ParseException {
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> verifyOTP = new HashMap<String, String>();
        verifyOTP.put("bank_id", bankId);
        verifyOTP.put("client_id", clientId);
        verifyOTP.put("client_txn_id", clientTxnId);
        verifyOTP.put("ref", refNumber);
        verifyOTP.put("otp", otp);
        JsonPath rb = given().contentType("application/json")
                .body(verifyOTP)
                .when().post("/check_otp/").jsonPath();
        String error_code = rb.get("error_code");
        return error_code;
    }

    public String VerifyOtpApiWithSimulator(String bankName) throws IOException, ParseException {
        JSONObject jo = JsonReader.getJsonInfo("BanksSimulator.json");
        String bankId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".bank_id");
        String clientId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".client_id");
        String cardNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".cardNumber");
        String phoneNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".phoneNumber");
        String refNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".ref");
        String idNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".idNumber");
        String otp = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".otp");

        if (verifyLinkIsExisted(bankId, clientId, refNumber)) {
            unLink(bankId, clientId, refNumber);
        }

        linkGiro(bankId, clientId, cardNumber, phoneNumber, refNumber, idNumber);

        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> verifyOTP = new HashMap<String, String>();
        verifyOTP.put("bank_id", bankId);
        verifyOTP.put("client_id", clientId);
        verifyOTP.put("ref", refNumber);
        verifyOTP.put("otp", otp);
        JsonPath rb = given().contentType("application/json")
                .body(verifyOTP)
                .when().post("/check_otp/").jsonPath();
        String error_code = rb.get("error_code");
//        String message = rb.get("message");
        return error_code;
    }

    /**
     * error_code 18 means existed link
     * error_code 37 means not existed link
     */
    public boolean verifyLinkIsExisted(String bankID, String clientID, String ref) {
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> cl = new HashMap<String, String>();
        cl.put("bank_id", bankID);
        cl.put("client_id", clientID);
        cl.put("ref", ref);
        boolean rb = given().contentType("application/json")
                .body(cl)
                .when().post("/check_link/").jsonPath().get("error_code").equals("18");
        return rb;
    }

    public String unLink(String bankID, String clientID, String ref) {
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> ul = new HashMap<String, String>();
        ul.put("bank_id", bankID);
        ul.put("client_id", clientID);
        ul.put("ref", ref);
        String rb = given().contentType("application/json")
                .body(ul)
                .when().post("/unlink/").jsonPath().get("error_code");
        return rb;
    }

    public JsonPath b2wGIRO(String bankId, String clientId, String clientTxnId, String refNumber, String amount) throws IOException, ParseException {
        /**Link if the link is not existed*/
        if (!verifyLinkIsExisted(bankId, clientId, refNumber)) {
            verifyLinkApiWithSimulator(BankName.VIB.toString());
            VerifyOtpApiWithSimulator(BankName.VIB.toString());
        }

        Demo.setupProxy();
        String description = "Khang is testing b2w";
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> link = new HashMap<String, String>();
        link.put("bank_id", bankId);
        link.put("client_id", clientId);
        link.put("client_txn_id", clientTxnId);
        link.put("ref", refNumber);
        link.put("amount", amount);
        link.put("description", description);
        return given().contentType("application/json")
                .body(link)
                .when().post("/bank2wallet/").jsonPath();
    }

    public String simulatorB2W(String bankName) throws IOException, ParseException {
        JSONObject jo = JsonReader.getJsonInfo("BanksSimulator.json");
        String bankId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".bank_id");
        String clientId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".client_id");
        String refNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".ref");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String clientTxnId = String.valueOf(timestamp.getTime());
        String amount = "10000";

        JsonPath jsonPath = b2wGIRO(bankId, clientId, clientTxnId, refNumber, amount);
        return jsonPath.get("error_code");
    }

    public void bankB2W(String bankId, String clientId, String clientTxnId, String refNumber, String amount, String errorCode) throws IOException, ParseException {
        JsonPath jsonPath = b2wGIRO(bankId, clientId, clientTxnId, refNumber, amount);
        Assert.assertEquals(jsonPath.get("error_code"), errorCode);
    }

    public JsonPath w2bGIRO(String bankId, String clientId, String clientTxnId, String refNumber, String amount) throws IOException, ParseException {
        /**Link if the link is not existed*/
        if (!verifyLinkIsExisted(bankId, clientId, refNumber)) {
            verifyLinkApiWithSimulator(BankName.VIB.toString());
            VerifyOtpApiWithSimulator(BankName.VIB.toString());
        }

        Demo.setupProxy();
        String description = "Khang is testing w2b";
        RestAssured.baseURI = "http://giro.airpay.vn";
        Map<String, String> link = new HashMap<String, String>();
        link.put("bank_id", bankId);
        link.put("client_id", clientId);
        link.put("client_txn_id", clientTxnId);
        link.put("ref", refNumber);
        link.put("amount", amount);
        link.put("description", description);
        return given().contentType("application/json")
                .body(link)
                .when().post("/wallet2bank/").jsonPath();
    }

    public String simulatorW2B(String bankName) throws IOException, ParseException {
        JSONObject jo = JsonReader.getJsonInfo("BanksSimulator.json");
        String bankId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".bank_id");
        String clientId = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".client_id");
        String refNumber = com.jayway.jsonpath.JsonPath.read(jo, "$." + bankName + ".ref");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String clientTxnId = String.valueOf(timestamp.getTime());
        String amount = "20000";

        JsonPath jsonPath = w2bGIRO(bankId, clientId, clientTxnId, refNumber, amount);
        return jsonPath.get("error_code");
    }
}
